<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Upcoming Matches</title>

  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <!-- Bootstrap theme -->
  <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="/theme.css" rel="stylesheet">
  <link href="/taterset.css" rel="stylesheet">
</head>
<body role="document">
<?php
  include '../restrict/navbar';
?>
<div class="container theme-showcase" role="main">
<div class="row">
<p>Disclaimer: These odds are estimations and may or may not represent the true probability of the outcome. We have provided a metric for how reliable we think our prediction is, but ultimately it is your decision on whether you agree with them. We strongly advise doing additional research on top of using the information available here.</p>
<h2>Upcoming Matches</h2>
<?php

  $star_en_url = '/img/star_enabled.png';
  $star_dis_url = '/img/star_disabled.png';
  $config = parse_ini_file('../restrict/dbconfig.ini',true);
  $dbh = new mysqli("localhost", $config['config_csgo']['user'], $config['config_csgo']['pass'], $config['config_csgo']['dbname']);
  if ($dbh->connect_errno){
    echo "Failed to connect to database";
  }
  function customError($errno, $errstr){
    echo "<b>Error:</b> [$errno] $errstr<br>";
    die();
  }
  set_error_handler("customError");

  $stmt = $dbh->prepare("SELECT * FROM upcoming_matches ORDER BY stars DESC");
  $stmt->execute();
  $res = $stmt->get_result();

  if (0 == $res->num_rows){
    echo "<h3>No upcoming matches.</h3>";
  } else {
    echo '<div class="col-xs-12 col-md-12 col-sm-12"><table class="table table-striped">';
    echo '<tr><th>Reliability (out of 5)</th><th>Best of<th>Lounge Odds</th><th>Our Estimate</th><th></th><th></th></tr>';
    while($row = $res->fetch_assoc()){
      echo '<tr><td>';
      if ($row['stars'] == 0){
        echo 'Unreliable';
      } else {
        for ($i = 0; $i < $row['stars']; ++$i){
          echo "<img src=\"$star_en_url\">";
        }
      }
      echo '</td><td class="centred">'.$row['best_of'].'</td><td><b>'.$row['team_1'].'</b>&emsp;'.round($row['csgl_odds']).'% - '.(100-round($row['csgl_odds'])).'%&emsp;<b>'.$row['team_2'].'</b></td><td><b>'.$row['team_1'].'</b>&emsp;'.round($row['our_odds']).'% - '.(100-round($row['our_odds'])).'%&emsp;<b>'.$row['team_2'].'</b></td><td><a href="/csgo/?team1='.urlencode($row['team_1']).'&team2='.urlencode($row['team_2']).'">Taterset</a></td><td><a href="'.$row['csgl_link'].'">CSGL</a></td></tr>';
    }
    echo "</table></div>";
  }

?>
</div>
<div class="row">
<h2>Live Matches</h2>
<?php
  $stmt = $dbh->prepare("SELECT * FROM live_matches ORDER BY stars DESC");
  $stmt->execute();
  $res = $stmt->get_result();

  if (0 == $res->num_rows){
    echo "<h3>No live matches.</h3>";
  } else {
    echo '<div class="col-xs-12 col-md-12 col-sm-12"><table class="table table-striped">';
    echo '<tr><th>Reliability (out of 5)</th><th>Best of<th>Lounge Odds</th><th>Our Estimate</th><th></th><th></th></tr>';
    while($row = $res->fetch_assoc()){
      echo '<tr><td>';
      if ($row['stars'] == 0){
        echo 'Unreliable';
      } else {
        for ($i = 0; $i < $row['stars']; ++$i){
          echo "<img src=\"$star_en_url\">";
        }
      }
      echo '</td><td class="centred">'.$row['best_of'].'</td><td><b>'.$row['team_1'].'</b>&emsp;'.round($row['csgl_odds']).'% - '.(100-round($row['csgl_odds'])).'%&emsp;<b>'.$row['team_2'].'</b></td><td><b>'.$row['team_1'].'</b>&emsp;'.round($row['our_odds']).'% - '.(100-round($row['our_odds'])).'%&emsp;<b>'.$row['team_2'].'</b></td><td><a href="/csgo/?team1='.urlencode($row['team_1']).'&team2='.urlencode($row['team_2']).'">Taterset</a></td><td><a href="'.$row['csgl_link'].'">CSGL</a></td></tr>';
    }
    echo "</table></div>";
  }
?>
</div>
<div class="row">
<h2>Past Matches</h2>
<?php
  $stmt = $dbh->prepare("SELECT * FROM past_matches ORDER BY stars DESC");
  $stmt->execute();
  $res = $stmt->get_result();

  if (0 == $res->num_rows){
    echo "<h3>No past matches.</h3>";
  } else {
    echo '<div class="col-xs-12 col-md-12 col-sm-12"><table class="table table-striped">';
    echo '<tr><th>Reliability (out of 5)</th><th>Best of<th>Lounge Odds</th><th>Our Estimate</th><th></th><th></th></tr>';
    while($row = $res->fetch_assoc()){
      echo '<tr><td>';
      if ($row['stars'] == 0){
        echo 'Unreliable';
      } else {
        for ($i = 0; $i < $row['stars']; ++$i){
          echo "<img src=\"$star_en_url\">";
        }
      }
      $winner = $row['winner'];
      echo '</td><td class="centred">'.$row['best_of'].'</td><td><b '.($row['winner'] == 1 ? 'style="color:green">' : 'style="color:red">').$row['team_1'].($winner == 1 ? '&#x2714' : '&#x2718;').'</b>&emsp;'.round($row['csgl_odds']).'% - '.(100-round($row['csgl_odds'])).'%&emsp;<b '.($row['winner'] == 2 ? 'style="color:green">&#x2714;' : 'style="color:red">&#x2718;').$row['team_2'].'</b></td><td><b>'.$row['team_1'].'</b>&emsp;'.round($row['our_odds']).'% - '.(100-round($row['our_odds'])).'%&emsp;<b>'.$row['team_2'].'</b></td><td><a href="/csgo/?team1='.urlencode($row['team_1']).'&team2='.urlencode($row['team_2']).'">Taterset</a></td><td><a href="'.$row['csgl_link'].'">CSGL</a></td></tr>';
    }
    echo "</table></div>";
  }
?>
</div>
</div>
  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/docs.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
