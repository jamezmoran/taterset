<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>CSGO Calculator</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
  $(function(){
    var availableTeams = [
    <?php
    $config = parse_ini_file('../restrict/dbconfig.ini',true);
    $dbh = new mysqli("localhost", $config['config_csgo']['user'], $config['config_csgo']['pass'], $config['config_csgo']['dbname']);
    if ($dbh->connect_errno){
      echo "Failed to connect to database";
    }
    $teams_stmt = $dbh->prepare(
      'SELECT team.team_name 
      FROM ratings_overall 
      INNER JOIN team 
      ON ratings_overall.team_id=team.id');
    $teams_stmt->execute();
    $res = $teams_stmt->get_result();
    $first_row = 1;
    while ($row = $res->fetch_assoc()){
      if ($first_row == 0){
        echo ',';
      } else {
        $first_row = 0;
      }
      echo '"'.$row['team_name'].'"';
    }
    ?>
    ];
    $( ".tags" ).autocomplete({
      source: availableTeams
    });
  });
  </script>
  <!-- Bootstrap core CSS -->
  <link href="http://taterset.com/css/bootstrap.min.css" rel="stylesheet">
  <!-- Bootstrap theme -->
  <link href="http://taterset.com/css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="http://taterset.com/theme.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="http://www.taterset.com/taterset.css">
</head>
<body role="document">
<?php
  include '../restrict/navbar';
?>
<div class="container theme-showcase" role="main">
<div class="centred">
<h1>CS:GO Odds Calculator</h1>
<p>Type the name of the teams you want to calculate odds for in the below boxes. The team names are case insensitive, but have to be spelled the same as they are on HLTV.</p>
<form class="ui_widget" action="." method="get">
  <?php 
    if (isset($_REQUEST['team1'])){
      echo "Team 1: <input class='tags' type=\"text\" name=\"team1\" value=\"".htmlspecialchars($_REQUEST['team1'])."\"><br>";
    } else {
      echo "Team 1: <input class='tags' type=\"text\" name=\"team1\"><br>";
    }
    if (isset($_REQUEST['team2'])){
      echo "Team 2: <input class='tags' type=\"text\" name=\"team2\" value=\"".htmlspecialchars($_REQUEST['team2'])."\"><br>";
    } else {
      echo "Team 2: <input class='tags' type=\"text\" name=\"team2\"><br>";
    }
  ?>
  <input type="submit">
</form>
</div>
<?php
  function customError($errno, $errstr){
    echo "<b>Error:</b> [$errno] $errstr<br>";
    die();
  }
  set_error_handler("customError");
  function g($deviation){
    return 1 / sqrt(1 + 3*$deviation*$deviation/(pi()*pi()));
  }
  function getProbability($rating_1, $rating_2, $deviation_2){
    return 1/(1 + exp(0 - g($deviation_2)*($rating_1-$rating_2)));
  }

  if (isset($_REQUEST['team1']) and isset($_REQUEST['team2'])){
    $team_1 = $_REQUEST['team1'];
    $team_2 = $_REQUEST['team2'];
    $stmt = $dbh->prepare(
      "SELECT 
        team.team_name, 
        rating, 
        rating_deviation, 
        matches 
      FROM 
        ratings_overall
        INNER JOIN team ON team.id=ratings_overall.team_id
      WHERE team_name=?");

    $team_name_bind = $team_1;
    $stmt->bind_param('s', $team_name_bind);
    $stmt->execute();
    $res = $stmt->get_result();
    if (0 == $res->num_rows){
      echo "<h3>Error: Could not find team ".htmlspecialchars($team_1)." in database</h3>";
    }
    $row = $res->fetch_assoc();
    $name_1 = $row['team_name'];
    $rating_1 = round($row['rating']);
    $deviation_1 = round($row['rating_deviation']);
    $matches_1 = $row['matches'];

    $team_name_bind = $team_2;
    $stmt->execute();
    $res = $stmt->get_result();
    if (0 == $res->num_rows){
      echo "<h3>Error: Could not find team ".htmlspecialchars($team_2)." in database</h3>";
    }
    $row = $res->fetch_assoc();
    $name_2 = $row['team_name'];
    $rating_2 = round($row['rating']);
    $deviation_2 = round($row['rating_deviation']);
    $matches_2 = $row['matches'];

    $rating_1_g = ($rating_1 - 1500)/173.7178;
    $rating_2_g = ($rating_2 - 1500)/173.7178;
    $deviation_2_g = $deviation_2 / 173.7178;
    $bo1_win = getProbability($rating_1_g, $rating_2_g,  $deviation_2_g);
    $bo2_odds = $bo1_win*$bo1_win/($bo1_win*$bo1_win + (1-$bo1_win)*(1-$bo1_win));
    $bo2_win = 
      $bo1_win * $bo1_win;
    $bo2_draw = 
    $bo3_win = 
      $bo1_win * $bo1_win * $bo1_win
      + 3 * ($bo1_win * $bo1_win * (1 - $bo1_win));
    $bo5_win =
      pow($bo1_win,5)
      + 5 * pow($bo1_win,4) * (1-$bo1_win)
      + 10 * pow($bo1_win,3) * pow((1-$bo1_win),2);
    $bo7_win =
      pow($bo1_win,7)
      + 7 * pow($bo1_win,6) * (1-$bo1_win)
      + 21 * pow($bo1_win,5) * pow((1-$bo1_win),2)
      + 35 * pow($bo1_win,4) * pow((1-$bo1_win),3);

    echo '<div class="col-md-16">';
    echo '<table id="matchup_table" class="table">';
    echo "<tr><th class=\"matchup_header\">$name_1</th><th class=\"matchup_header\">vs.</th><th class=\"matchup_header\">$name_2</th></tr>";
    echo "<tr>";

    echo "<td>";
    echo "Rating: $rating_1 &plusmn; ".(2*$deviation_1)."<br>";
?>
<div class="row">
  <h3>Our Estimation</h3>
  <div class="col-md-12">
    <table class="table table-striped">
      <tr><th></th><th>BO1</th><th>BO2</th><th>BO3</th><th>BO5</th><th>BO7</th></tr>
      <tr><td>Win%</td>
      <?php
        echo "<td>".round($bo1_win*100,1)."%</td>";
        echo "<td>".round($bo2_win*100,1)."%</td>";
        echo "<td>".round($bo3_win*100,1)."%</td>";
        echo "<td>".round($bo5_win*100,1)."%</td>";
        echo "<td>".round($bo7_win*100,1)."%</td>";
      ?>
      </tr>
      <tr><td>Win% Ignoring Draw</td>
      <?php
        echo '<td>-</td><td>'.round($bo2_odds*100,1).'%</td><td>-</td><td>-</td><td>-</td>';
      ?>
      </tr>
    </table>
  </div>
</div>

<?php

    echo "<h3>Most recent matches</h3>";

    $stmt_matches_param = $name_1;
    $stmt_matches = $dbh->prepare("
    SELECT 
      map_name,
      t1.team_name AS team_name_1,
      team_score_1,
      t2.team_name AS team_name_2,
      team_score_2,
      DATE_FORMAT(date,'%d-%m-%Y') AS date_string,
      date,
      hltv_link
    FROM
      matches
      INNER JOIN team AS t1 ON t1.id=matches.team_1
      INNER JOIN team AS t2 ON t2.id=matches.team_2
    WHERE 
      t1.team_name=?
      OR t2.team_name=?
    ORDER BY date DESC
    LIMIT 20");
    $stmt_matches->bind_param('ss',$stmt_matches_param,$stmt_matches_param);
    $stmt_matches->execute();
    $res = $stmt_matches->get_result();
    echo '<table class="table">';
    while($row = $res->fetch_assoc()){
      $win = 0;
      if ($row['team_name_1'] == $name_1 && $row['team_score_1'] > $row['team_score_2']){
        $win = 1;
      } else if ($row['team_name_2'] == $name_1 && $row['team_score_2'] > $row['team_score_1']){
        $win = 1;
      }
      echo '<tr class="'.($win ? 'match_win' : 'match_loss').'"><td><a href="http://www.hltv.org'.$row['hltv_link'].'">'.$row['date_string'].'</a></td><td>'.$row['team_name_1'].'</td><td>'.$row['team_score_1'].' - '.$row['team_score_2'].'</td><td>'.$row['team_name_2'].'</td><td>'.$row['map_name'].'</td></tr>';

    }
    echo '</table>';


    echo "</td>";
    echo '<td></td>';

    echo "<td>";
    echo "Rating: $rating_2 &plusmn; ".(2*$deviation_2)."<br>";
?>
<div class="row">
  <h3>Our Estimation</h3>
  <div class="col-md-12">
    <table class="table table-striped">
      <tr><th></th><th>BO1</th><th>BO2</th><th>BO3</th><th>BO5</th><th>BO7</th></tr>
      <tr><td>Win%</td>
      <?php
        echo "<td>".round((1-$bo1_win)*100,1)."%</td>";
        echo "<td>".round((1-$bo1_win)*(1-$bo1_win)*100,1)."%</td>";
        echo "<td>".round((1-$bo3_win)*100,1)."%</td>";
        echo "<td>".round((1-$bo5_win)*100,1)."%</td>";
        echo "<td>".round((1-$bo7_win)*100,1)."%</td>";
      ?>
      </tr>
      <tr><td>Win% Ignoring Draw</td>
      <?php
        echo '<td>-</td><td>'.round((1-$bo2_odds)*100,1).'%</td><td>-</td><td>-</td><td>-</td>';
      ?>
      </tr>
    </table>
  </div>
</div>

<?php
    echo "<h3>Most recent matches</h3>";
    $stmt_matches_param = $name_2;
    $stmt_matches->execute();
    $res = $stmt_matches->get_result();
    echo '<table class="table">';
    while($row = $res->fetch_assoc()){
      $win = 0;
      if ($row['team_name_1'] == $name_2 && $row['team_score_1'] > $row['team_score_2']){
        $win = 1;
      } else if ($row['team_name_2'] == $name_2 && $row['team_score_2'] > $row['team_score_1']){
        $win = 1;
      }
      echo '<tr class="'.($win ? 'match_win' : 'match_loss').'"><td><a href="http://www.hltv.org'.$row['hltv_link'].'">'.$row['date_string'].'</a></td><td>'.$row['team_name_1'].'</td><td>'.$row['team_score_1'].' - '.$row['team_score_2'].'</td><td>'.$row['team_name_2'].'</td><td>'.$row['map_name'].'</td></tr>';

    }
    echo '</table>';
    echo "</td>";
    echo "</tr>";
    echo "</table>";
    echo '</div>';
  }
  
?>
</div>
  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://taterset.com/js/bootstrap.min.js"></script>
  <script src="http://taterset.com/js/docs.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="http://taterset.com/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
