
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>CSGO Rankings</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <!-- Bootstrap theme -->
  <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="/theme.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="http://www.taterset.com/taterset.css">
</head>
<body role="document">
<?php
  include '../restrict/navbar';
?>
<div class="container theme-showcase" role="main">
  <div class="row">
    <div class="col-xs-12 col-md-12 col-sm-12">
      <h1>CS:GO Team Rankings</h1>
      <p>Teams must have a rating deviation of <b>less than 55</b> to be listed here.
      <br>Teams are conservatively ordered by their 'Rating' subtracted by their two times their 'Rating Deviation' (R - 2*Rd). </p>
    </div>
  </div>
  <div class="row" style="min-width:1100px">
    <div class="col-xs-4 col-md-4 col-sm-4">
      <table class="table table-striped">
        <tr><th>Rank</th><th>Team</th><th title='Conservative Rating (Rating - RD*2)'><u style="border-bottom:1px dotted #000;text-decoration:none;">C.Rating</u></th><th>Rating</th><th title='Rating Deviation'><u style="border-bottom:1px dotted #000;text-decoration:none;">RD</u></th></tr>
        <?php
          $config = parse_ini_file('../restrict/dbconfig.ini',true);
          $dbh = new mysqli("localhost", $config['config_csgo']['user'], $config['config_csgo']['pass'], $config['config_csgo']['dbname']);
          if ($dbh->connect_errno){
            echo "Failed to connect to database";
          }
          $teams_stmt = $dbh->prepare(
            'SELECT 
              team.team_name AS team, 
              rating AS rating,
              rating_deviation
            FROM ratings_overall 
            INNER JOIN team 
            ON ratings_overall.team_id=team.id
            WHERE rating_deviation < 55
            ORDER BY rating-2*rating_deviation DESC');
          $teams_stmt->execute();
          $res = $teams_stmt->get_result();
          $count = 0;
          while ($row = $res->fetch_assoc()){
            $count++;
            $team = htmlspecialchars($row['team']);
            $conservative_rating = round($row['rating']-2*$row['rating_deviation']);
            $rating = round($row['rating']);
            $rating_deviation = round($row['rating_deviation']);
            echo "<tr><td>$count.</td><td><a href='rankings.php?team=".urlencode($team)."'>".$team."</a></td><td>$conservative_rating</td><td>$rating</td><td>$rating_deviation</td></tr>";
          }
        ?>
      </table>
    </div>
    <div class="col-xs-6 col-md-6 col-sm-6">
    <?php
      if (isset($_REQUEST['team'])){
        $team = $_REQUEST['team']; 
      
        echo '<h3>Most recent matches for '.htmlspecialchars($team).'</h3>';

        $stmt_matches_param = $team;
        $stmt_matches = $dbh->prepare("
        SELECT 
          map_name,
          t1.team_name AS team_name_1,
          team_score_1,
          t2.team_name AS team_name_2,
          team_score_2,
          DATE_FORMAT(date,'%d-%m-%Y') AS date_string,
          date,
          hltv_link
        FROM
          matches
          INNER JOIN team AS t1 ON t1.id=matches.team_1
          INNER JOIN team AS t2 ON t2.id=matches.team_2
        WHERE 
          t1.team_name=?
          OR t2.team_name=?
        ORDER BY date DESC
        LIMIT 30");
        $stmt_matches->bind_param('ss',$stmt_matches_param,$stmt_matches_param);
        $stmt_matches->execute();
        $res = $stmt_matches->get_result();
        echo '<table class="table centred">';
        while($row = $res->fetch_assoc()){
          $win = 0;
          if ($row['team_name_1'] == $team && $row['team_score_1'] > $row['team_score_2']){
            $win = 1;
          } else if ($row['team_name_2'] == $team && $row['team_score_2'] > $row['team_score_1']){
            $win = 1;
          }
          echo '<tr class="'.($win ? 'match_win' : 'match_loss').'"><td><a href="http://www.hltv.org'.$row['hltv_link'].'">'.$row['date_string'].'</a></td><td>'.htmlspecialchars($row['team_name_1']).'</td><td>'.$row['team_score_1'].' - '.$row['team_score_2'].'</td><td>'.$row['team_name_2'].'</td><td>'.$row['map_name'].'</td></tr>';

        }
        echo '</table>';
      }
    ?>
    </div>
  </div>
</div>
  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/docs.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
