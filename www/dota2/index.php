<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dota 2 Calculator</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" type="text/css" href="http://www.taterset.com/taterset.css">
  <script>
  $(function(){
    var availableTeams = [
    <?php
    $dbh = new mysqli("localhost", "taterset_reader", "TvyguRB[@uD!", "taterset_dota2");
    if ($dbh->connect_errno){
      echo "Failed to connect to database";
    }
    $teams_stmt = $dbh->prepare('SELECT team_name FROM ratings_overall');
    $teams_stmt->execute();
    $res = $teams_stmt->get_result();
    $first_row = 1;
    while ($row = $res->fetch_assoc()){
    if ($first_row == 0){
    echo ',';
    } else {
    $first_row = 0;
    }
    echo '"'.$row['team_name'].'"';
    }
    ?>
    ];
    $( ".tags" ).autocomplete({
    source: availableTeams
    });
  });
  </script>
  <!-- Bootstrap core CSS -->
  <link href="http://taterset.com/css/bootstrap.min.css" rel="stylesheet">
  <!-- Bootstrap theme -->
  <link href="http://taterset.com/css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="http://taterset.com/theme.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="http://www.taterset.com/taterset.css">
</head>
<body>
<?php
  include '../restrict/navbar';
?>
<div class="container theme-showcase" role="main">
<div class="centred">
<h1>Dota 2 Odds Calculator</h1>
<p>In the below boxes, type the name of the teams you want to calculate odds for. The team names are case insensitive, but have to be spelled the same as they are on datdota.</p>
<form class="ui_widget" action="." method="post">
  <?php 
    if (isset($_POST['team1'])){
      echo "Team 1: <input class='tags' type=\"text\" name=\"team1\" value=\"".htmlspecialchars($_POST['team1'])."\"><br>";
    } else {
      echo "Team 1: <input class='tags' type=\"text\" name=\"team1\"><br>";
    }
    if (isset($_POST['team2'])){
      echo "Team 2: <input class='tags' type=\"text\" name=\"team2\" value=\"".htmlspecialchars($_POST['team2'])."\"><br>";
    } else {
      echo "Team 2: <input class='tags' type=\"text\" name=\"team2\"><br>";
    }
  ?>
  <input type="submit">
</form>
</div>
<?php
  function customError($errno, $errstr){
    echo "<b>Error:</b> [$errno] $errstr<br>";
    die();
  }
  set_error_handler("customError");
  function g($deviation){
    return 1 / sqrt(1 + 3*$deviation*$deviation/(pi()*pi()));
  }
  function getProbability($rating_1, $rating_2, $deviation_2){
    return 1/(1 + exp(0 - g($deviation_2)*($rating_1-$rating_2)));
  }

  if (isset($_POST['team1']) and isset($_POST['team2'])){
    $team_1 = $_POST['team1'];
    $team_2 = $_POST['team2'];
    $stmt = $dbh->prepare("SELECT team_name, rating, rating_deviation, matches FROM ratings_overall WHERE team_name=?");

    $team_name_bind = $team_1;
    $stmt->bind_param('s', $team_name_bind);
    $stmt->execute();
    $res = $stmt->get_result();
    if (0 == $res->num_rows){
      echo "<h3>Error: Could not find team ".htmlspecialchars($team_1)." in database</h3>";
    }
    $row = $res->fetch_assoc();
    $name_1 = $row['team_name'];
    $rating_1 = $row['rating'];
    $deviation_1 = $row['rating_deviation'];
    $matches_1 = $row['matches'];

    $team_name_bind = $team_2;
    $stmt->execute();
    $res = $stmt->get_result();
    if (0 == $res->num_rows){
      echo "<h3>Error: Could not find team ".htmlspecialchars($team_2)." in database</h3>";
    }
    $row = $res->fetch_assoc();
    $name_2 = $row['team_name'];
    $rating_2 = $row['rating'];
    $deviation_2 = $row['rating_deviation'];
    $matches_2 = $row['matches'];

    $stars_1 = 1;
    $stars_2 = 1;

    if ($deviation_1 < 70){
      $stars_1++;
    }
    if ($deviation_1 < 80){
      $stars_1++;
    }
    if ($deviation_1 < 90){
      $stars_1++;
    }
    if ($matches_1 > 20){
      $stars_1++;
    }
    if ($matches_1 > 40){
      $stars_1++;
    }
    if ($matches_1 > 60){
      $stars_1++;
    }
    if ($matches_1 > 80){
      $stars_1++;
    }
    if ($matches_1 > 100){
      $stars_1++;
    }
    if ($matches_1 > 120){
      $stars_1++;
    }


    if ($deviation_2 < 70){
      $stars_2++;
    }
    if ($deviation_2 < 80){
      $stars_2++;
    }
    if ($deviation_2 < 90){
      $stars_2++;
    }
    if ($matches_2 > 20){
      $stars_2++;
    }
    if ($matches_2 > 40){
      $stars_2++;
    }
    if ($matches_2 > 60){
      $stars_2++;
    }
    if ($matches_2 > 80){
      $stars_2++;
    }
    if ($matches_2 > 100){
      $stars_2++;
    }
    if ($matches_2 > 120){
      $stars_2++;
    }
    $rating_1_g = ($rating_1 - 1500)/173.7178;
    $rating_2_g = ($rating_2 - 1500)/173.7178;
    $deviation_2_g = $deviation_2 / 173.7178;
    $bo1_win = getProbability($rating_1_g, $rating_2_g,  $deviation_2_g);
    $bo2_win = 
      $bo1_win * $bo1_win;
    $bo3_win = 
      $bo1_win * $bo1_win * $bo1_win
      + 3 * ($bo1_win * $bo1_win * (1 - $bo1_win));
    $bo5_win =
      pow($bo1_win,5)
      + 5 * pow($bo1_win,4) * (1-$bo1_win)
      + 10 * pow($bo1_win,3) * pow((1-$bo1_win),2);

    echo '<div class="col-md-12">';
    echo '<table id="matchup_table" class="table">';
    echo "<tr><th class=\"matchup_header\">$name_1</th><th class=\"matchup_header\">vs.</th><th class=\"matchup_header\">$name_2</th></tr>";
    echo "<tr>";

    echo "<td>";
    echo "$name_1 - $rating_1<br>";
    if ($stars_1 > 8){
      echo "$name_1 rating very reliable. $stars_1/10 ($matches_1 matches)\n";
    } else if ($stars_1 > 6){
      echo "$name_1 rating reliable. $stars_1/10 ($matches_1 matches)\n";
    } else if ($stars_1 > 4){
      echo "$name_1 rating semi-reliable. $stars_1/10 ($matches_1 matches)\n";
    } else if ($stars_1 > 2){
      echo "$name_1 rating unreliable. $stars_1/10 ($matches_1 matches)\n";
    } else {
      echo "$name_1 rating very unreliable. $stars_1/10 ($matches_1 matches)\n";
    }
    echo "<h3>Our Odds Prediction</h3>";
    echo "BO1:<br>".round($bo1_win*100,1)."% Win<br><br>";
    echo "BO3:<br>".round($bo3_win*100,1)."% Win<br><br>";
    echo "BO5:<br>".round($bo5_win*100,1)."% Win";

    echo "<h3>Most recent matches</h3>";

    $stmt_matches_param = $name_1;
    $stmt_matches = $dbh->prepare("
    SELECT 
      team_name_1,
      team_score_1,
      team_name_2,
      team_score_2,
      DATE_FORMAT(date,'%d-%m-%Y') AS date_string,
      date
    FROM
      matches
    WHERE 
      team_name_1=?
      OR team_name_2=?
    ORDER BY date DESC
    LIMIT 20");
    $stmt_matches->bind_param('ss',$stmt_matches_param,$stmt_matches_param);
    $stmt_matches->execute();
    $res = $stmt_matches->get_result();
    echo '<table class="table">';
    while($row = $res->fetch_assoc()){
      $win = 0;
      if ($row['team_name_1'] == $name_1 && $row['team_score_1'] > $row['team_score_2']){
        $win = 1;
      } else if ($row['team_name_2'] == $name_1 && $row['team_score_2'] > $row['team_score_1']){
        $win = 1;
      }
      echo '<tr class="'.($win ? 'match_win' : 'match_loss').'"><td>'.$row['date_string'].'</td><td>'.$row['team_name_1'].'</td><td>'.$row['team_score_1'].' - '.$row['team_score_2'].'</td><td>'.$row['team_name_2'].'</td><td></tr>';

    }
    echo '</table>';


    echo "</td>";
    echo '<td style="background-color:white"></td>';

    echo "<td>";
    echo "$name_2 - $rating_2<br>";
    if ($stars_2 > 8){
      echo "$name_2 rating very reliable. $stars_2/10 ($matches_2 matches)\n";
    } else if ($stars_2 > 6){
      echo "$name_2 rating reliable. $stars_2/10 ($matches_2 matches)\n";
    } else if ($stars_2 > 4){
      echo "$name_2 rating semi-reliable. $stars_2/10 ($matches_2 matches)\n";
    } else if ($stars_2 > 2){
      echo "$name_2 rating unreliable. $stars_2/10 ($matches_2 matches)\n";
    } else {
      echo "$name_2 rating very unreliable. $stars_2/10 ($matches_2 matches)\n";
    }
    echo "<h3>Our Odds Prediction</h3>";
    echo "BO1:<br>".round((1-$bo1_win)*100,1)."% Win<br><br>";
    echo "BO3:<br>".round((1-$bo3_win)*100,1)."% Win<br><br>";
    echo "BO5:<br>".round((1-$bo5_win)*100,1)."% Win";

    echo "<h3>Most recent matches</h3>";
    $stmt_matches_param = $name_2;
    $stmt_matches->execute();
    $res = $stmt_matches->get_result();
    echo '<table class="table">';
    while($row = $res->fetch_assoc()){
      $win = 0;
      if ($row['team_name_1'] == $name_2 && $row['team_score_1'] > $row['team_score_2']){
        $win = 1;
      } else if ($row['team_name_2'] == $name_2 && $row['team_score_2'] > $row['team_score_1']){
        $win = 1;
      }
      echo '<tr class="'.($win ? 'match_win' : 'match_loss').'"><td>'.$row['date_string'].'</td><td>'.$row['team_name_1'].'</td><td>'.$row['team_score_1'].' - '.$row['team_score_2'].'</td><td>'.$row['team_name_2'].'</td><td></tr>';

    }
    echo '</table>';
    echo "</td>";
    echo "</tr>";
    echo "</table>";
    echo '</div>';
  }
  
?>
</div>
  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://taterset.com/js/bootstrap.min.js"></script>
  <script src="http://taterset.com/js/docs.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="http://taterset.com/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
