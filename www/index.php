<!DOCTYPE html>
<html>
<head>
  <title>taterset</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/theme.css" rel="stylesheet">
</head>
<body role="document">
  <?php
    include 'restrict/navbar'; 
  ?>
  <div class="container theme-showcase" role="main">
    <div class="jumbotron">
    <h1>Welcome to taterset</h1>
    <p>This is a website dedicated to providing objective analysis of eSports matches and generating accurate ratings for professional teams. The information published here is constructed from an exhaustive data analysis of every team's match history, entered into an advanced rating system, and then weighted to provide what we consider to be fairly accurate odds for team match-ups. As a very new site developed by a single person (me), it currently only provides raw estimations for matchups. As time goes on, I expect to expand the depth of the information available with an easy-to-use, no nonsense interface.</p>
    </div>
    
  </div>
  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/docs.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>

