use warnings;
use strict;

use DBI;
use LWP::UserAgent;
use HTML::TreeBuilder;
use Data::Dumper;
use POSIX qw(strftime);

sub str2time {
  my $time = shift;
  my $month_hash = {
    Jan => 1,
    Feb => 2,
    Mar => 3,
    Apr => 4,
    May => 5,
    Jun => 6,
    Jul => 7,
    Aug => 8,
    Sep => 9,
    Oct => 10,
    Nov => 11,
    Dec => 12,
  };
  my @parts = split(' ', $time);
  my $month = $month_hash->{$parts[0]};
  my $day = $parts[1];
  $day =~ s/,//;
  my $year = $parts[2];

  return $year*10000 + $month * 100 + $day;
}

my ($online_type, $lan_type, $majors_type) = (0,1,2);

my $database = 'taterset_csgo';
my $hostname = 'localhost';
my $dsn = "dbi:mysql:database=$database;host=$hostname;port=3306";
my $dbh = DBI->connect($dsn,"taterset_james","4D}f;%x~^g1#");
my $ua = LWP::UserAgent->new();
$ua->agent('Mozilla/5.0');

my $maps = {
  de_cache => 29,
  de_season => 30,
  de_dust2 => 31,
  de_mirage => 32,
  de_inferno => 33,
  de_nuke => 34,
  de_train => 35,
  de_cobblestone => 39,
  de_overpass => 40,
  de_tuscan => 42
};

my $do_best_ofs = 0;

my $time = time;
my $day_secs = 86400;
my $fortnight_secs = 1209600;
my $halfyear_secs = 1.57785e7;
my $change_count = 0;
WHOLE_LOOP: for (my $t = $time+$day_secs; $t > ($time - $halfyear_secs); $t -= $fortnight_secs){
  my $params = {
    void => 'false',
    highlight => 'false',
    stats => 'false',
    demo => 'false'
  };
  my $from_time = strftime("%Y-%m-%d", localtime($t-$fortnight_secs));
  my $to_time = strftime("%Y-%m-%d", localtime($t-$day_secs));
  $params->{daterange} = "$from_time to $to_time";
  my $url = "http://www.hltv.org/?pageid=324&filter=1&clean=1";
  my $response = $ua->post($url, $params);
  if ($response->is_success){
    my $tree = HTML::TreeBuilder->new;
    $tree->parse($response->decoded_content);
    my @matches = $tree->look_down(_tag=>'a', href=>qr#^(/match/)#);
    for (my $i = 0; $i < scalar @matches; $i += 2) {
      my $hltv_link = $matches[$i]->attr('href');
      my $select_stmt = $dbh->prepare("SELECT 1 FROM matches WHERE hltv_link=? LIMIT 1");
      $select_stmt->execute($hltv_link);
      if ($select_stmt->rows() > 0){
        if ($change_count > 5){
          last WHOLE_LOOP;
        } else {
          $change_count++;
          next;
        }
      } else {
        $change_count = 0;
      }
      my $match_date = $matches[$i];
      my $match_result = $matches[$i+1];
      my $date = str2time($match_date->as_text);
      if (not ($match_result->as_text =~ /(.*)\svs\s\s(.*) \((\d\d?)-(\d\d?)\).*/)){
        next;
      }
      my $team_name_1 = $1;
      my $team_name_2 = $2;
      my $team_score_1 = $3;
      my $team_score_2 = $4;
      $team_name_1 =~ s/^(\s+)//;
      $team_name_1 =~ s/(\s+)$//;
      $team_name_2 =~ s/^(\s+)//;
      $team_name_2 =~ s/(\s+)$//;

      my $insert_team = $dbh->prepare("INSERT INTO team (team_name) VALUES (?) ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id)");
      my $insert_match = $dbh->prepare("INSERT INTO matches (map_name, team_1, team_score_1, team_2, team_score_2, date, hltv_link) VALUES (?,?,?,?,?,?,?)");
      my $insert_team_name = $dbh->prepare("INSERT IGNORE INTO team_names VALUES (?,?)");
      $insert_team->execute($team_name_1);
      my $team_1_id = $dbh->last_insert_id(undef,undef,undef,undef);
      $insert_team->execute($team_name_2);
      my $team_2_id = $dbh->last_insert_id(undef,undef,undef,undef);
      $insert_team_name->execute($team_name_1, $team_1_id);
      $insert_team_name->execute($team_name_2, $team_2_id);

      my $match_page = $ua->get("http://www.hltv.org$hltv_link");
      if ($match_page->is_success){
        my $match_tree = HTML::TreeBuilder->new;
        $match_tree->parse($match_page->decoded_content);

        my $hotmatchbox = $match_tree->look_down(class=>'hotmatchbox');
        my @imgs = $hotmatchbox->look_down(_tag=>'img', style=>qr/(;;)$/);
        my @spans = $hotmatchbox->look_down(_tag=>'span');
        if (scalar @imgs and scalar @spans){
          my $img = $imgs[0];
          if ($img->attr('src') =~ qr#/default\.png#){
            shift @imgs;
            shift @spans;
            shift @spans;
          }
          if (scalar @spans != (scalar @imgs)*6){
            print "$hltv_link\n";
            next;
          }
          for (my $j = 0; $j < scalar @imgs; ++$j){
            my $img = $imgs[$j];
            $img->attr('src') =~ qr#/([a-z0-9]+).png#;
            my $map = "de_$1";
            $team_score_1 = $spans[6*$j]->as_text;
            $team_score_2 = $spans[6*$j+1]->as_text;
            print "$hltv_link $date $map $team_name_1 $team_score_1 - $team_score_2 $team_name_2\n";
            $insert_match->execute($map, $team_1_id, $team_score_1, $team_2_id, $team_score_2, $date, $hltv_link);
          }
          $img->delete();
        }

      $match_tree->delete();
      }
    }
    $tree->delete();
  } else {
    print $response->status_line."\n";
  }
}
