use warnings;
use strict;

use DBI;
use LWP::UserAgent;
use HTML::TreeBuilder;
use Data::Dumper;
use Math::Trig;

sub g {
  my $o = shift;
  return 1 / sqrt(1 + 3*$o*$o/(pi*pi));
}
sub E {
  my ($u, $uj, $oj) = @_;
  return 1/(1 + exp(0 - g($oj)*($u-$uj)));
}

my ($team_1, $team_2) = @ARGV;
my $dbh = DBI->connect("dbi:SQLite:dbname=ratings_dota2","","");
my $stmt = $dbh->prepare("SELECT rating, rating_deviation, matches FROM ratings_overall WHERE team_name LIKE ?");
$stmt->execute($team_1);
my ($rating_1, $deviation_1, $matches_1) = $stmt->fetchrow_array;
$stmt->execute($team_2);
my ($rating_2, $deviation_2, $matches_2) = $stmt->fetchrow_array;

my $stars_1 = 1;
my $stars_2 = 1;

if ($deviation_1 < 70){
  $stars_1++;
}
if ($deviation_1 < 80){
  $stars_1++;
}
if ($deviation_1 < 90){
  $stars_1++;
}
if ($matches_1 > 20){
  $stars_1++;
}
if ($matches_1 > 40){
  $stars_1++;
}
if ($matches_1 > 60){
  $stars_1++;
}
if ($matches_1 > 80){
  $stars_1++;
}
if ($matches_1 > 100){
  $stars_1++;
}
if ($matches_1 > 120){
  $stars_1++;
}


if ($deviation_2 < 70){
  $stars_2++;
}
if ($deviation_2 < 80){
  $stars_2++;
}
if ($deviation_2 < 90){
  $stars_2++;
}
if ($matches_2 > 20){
  $stars_2++;
}
if ($matches_2 > 40){
  $stars_2++;
}
if ($matches_2 > 60){
  $stars_2++;
}
if ($matches_2 > 80){
  $stars_2++;
}
if ($matches_2 > 100){
  $stars_2++;
}
if ($matches_2 > 120){
  $stars_2++;
}


if ($stars_1 > 8){
  print "$team_1 odds very reliable. $stars_1/10 ($matches_1 matches)\n";
} elsif ($stars_1 > 6){
  print "$team_1 odds reliable. $stars_1/10 ($matches_1 matches)\n";
} elsif ($stars_1 > 4){
  print "$team_1 odds semi-reliable. $stars_1/10 ($matches_1 matches)\n";
} elsif ($stars_1 > 2){
  print "$team_1 odds unreliable. $stars_1/10 ($matches_1 matches)\n";
} else {
  print "$team_1 odds very unreliable. $stars_1/10 ($matches_1 matches)\n";
}
if ($stars_2 > 8){
  print "$team_2 odds very reliable. $stars_2/10 ($matches_2 matches)\n";
} elsif ($stars_2 > 6){
  print "$team_2 odds reliable. $stars_2/10 ($matches_2 matches)\n";
} elsif ($stars_2 > 4){
  print "$team_2 odds semi-reliable. $stars_2/10 ($matches_2 matches)\n";
} elsif ($stars_2 > 2){
  print "$team_2 odds unreliable. $stars_2/10 ($matches_2 matches)\n";
} else {
  print "$team_2 odds very unreliable. $stars_2/10 ($matches_2 matches)\n";
}

$rating_1 = ($rating_1 - 1500) / 173.7178;
$rating_2 = ($rating_2 - 1500) / 173.7178;
$deviation_2 = $deviation_2 / 173.7178;
my $raw_probability = E($rating_1, $rating_2, $deviation_2);
my $bo2_safe = (
  ($raw_probability*$raw_probability)
  + 2*($raw_probability * (1-$raw_probability))) * 100;
my $bo2_win = $raw_probability * $raw_probability * 100;
my $bo2_draw = 2*($raw_probability * (1-$raw_probability))*100;
my $bo3_win = (
  ($raw_probability * $raw_probability * $raw_probability)
  + 3 * ($raw_probability * $raw_probability * (1-$raw_probability))) * 100;
my $real_probability = $raw_probability * 100;
my $bo5_win = (
  10 * ($raw_probability**3 * (1-$raw_probability)**2)
  + 5 * ($raw_probability**4 * (1-$raw_probability))
  + $raw_probability**5)*100;
print("
$team_1 potential:
BO1:
\t$real_probability% Win
BO2:
\t$bo2_safe% Safe
\t$bo2_draw% Draw
\t$bo2_win% Win
BO3:
\t$bo3_win% Win
BO5:
\t$bo5_win% Win\n")
