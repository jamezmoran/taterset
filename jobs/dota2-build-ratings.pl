use warnings;
use strict;

use DBI;
use POSIX;
use LWP::UserAgent;
use HTML::TreeBuilder;
use Data::Dumper;
use Math::Trig;

sub g {
  my $o = shift;
  return 1 / sqrt(1 + 3*$o*$o/(pi*pi));
}
sub E {
  my ($u, $uj, $oj) = @_;
  return 1/(1 + exp(0 - g($oj)*($u-$uj)));
}

my $r = 0.6;
my $volatility = 0.06;
my ($online_type, $lan_type, $majors_type) = (0,1,2);

my %ratings;
my $database = 'taterset_dota2';
my $hostname = 'localhost';

my $dsn = "dbi:mysql:database=$database;host=$hostname;port=3306";
my $dbh = DBI->connect($dsn,"taterset_james","4D}f;%x~^g1#");

my $six_months_ago = strftime("%Y-%m-%d", localtime(time - 7.88923e6*2));
my $matches_stmt = $dbh->prepare(
  " SELECT *
    FROM matches
    WHERE date > '$six_months_ago'
    ORDER BY date ASC"
);
$matches_stmt->execute();
while (my ($id, $team_name_1, $team_score_1, $team_name_2, $team_score_2) = $matches_stmt->fetchrow_array){
  print "$id, $team_name_1, $team_score_1, $team_name_2, $team_score_2\n";
  sub calculate_rating {
    my ($team_name_1, $team_score_1, $team_name_2, $team_score_2, $params) = @_;
    my $team_points_1;
    my $team_points_2;
    if ($team_score_1 > $team_score_2){
      $team_points_1 = 1;
      $team_points_2 = 0;
    } elsif ($team_score_1 < $team_score_2) {
      $team_points_1 = 0;
      $team_points_2 = 1;
    } else {
      $team_points_1 = 0.5;
      $team_points_2 = 0.5;
    }

    my $matches = $params->{matches};
    my $team_r_1 = $params->{team_r_1};
    my $team_rd_1 = $params->{team_rd_1};
    my $team_v_1 = $params->{team_v_1};
    my $team_r_2 = $params->{team_r_2};
    my $team_rd_2 = $params->{team_rd_2};
    my $team_v_2 = $params->{team_v_2};


    print "old ratings: $team_r_1 $team_rd_1\n";
    my $team_r_1_g2 = ($team_r_1 - 1500) / 173.7178;
    my $team_rd_1_g2 = $team_rd_1 / 173.7178;
    my $team_r_2_g2 = ($team_r_2 - 1500) / 173.7178;
    my $team_rd_2_g2 = $team_rd_2 / 173.7178;

    my $gv = g($team_rd_2_g2);
    my $ev = E($team_r_1_g2, $team_r_2_g2, $team_rd_2_g2);
    my $v = 1 / ($gv*$gv*$ev*(1-$ev));
    print "v values: gv - $gv, ev - $ev, v - $v\n";


    my $delta = $v*$gv*($team_points_1 - $ev);
    print "delta: $delta\n";

    my ($a, $A, $b, $B);
    $a = $A = log($team_v_1*$team_v_1);
    sub f {
      my $x = shift;
      my $exp_x = exp($x);
      my $delta = shift;
      my $team_rd_1 = shift;
      my $v = shift;
      my $a = shift;
      return ($exp_x*($delta*$delta - $team_rd_1*$team_rd_1 - $v - $exp_x) 
        / (2 * ($team_rd_1*$team_rd_1 + $v + $exp_x)*($team_rd_1*$team_rd_1 + $v + $exp_x)))
      - ($x - $a)/($r*$r);
    }

    if ($delta*$delta > ($team_rd_1_g2*$team_rd_1_g2 + $v)){
      $B = log($delta*$delta - $team_rd_1_g2*$team_rd_1_g2 - $v);
    } else {
      my $k = 1;
      while (f($a - $k*$r, $delta, $team_rd_1_g2, $v, $a) < 0){
        $k++;
      }
      $B = $a - $k*$r;
    }

    my ($fa, $fb) = (f($A, $delta, $team_rd_1_g2, $v, $a), f($B, $delta, $team_rd_1_g2, $v, $a));
    while (abs($B - $A) > 0.000001){
      my $C = $A + ($A - $B)*$fa/($fb-$fa);
      my $fc = f($C, $delta, $team_rd_1_g2, $v, $a);
      if ($fc*$fb < 0){
        $A = $B;
        $fa = $fb;
      } else {
        $fa = $fa/2;
      }
      $B = $C;
      $fb = $fc;
    }

    my $v_new = exp($A/2);
    my $rd_new = sqrt($team_rd_1_g2*$team_rd_1_g2 + $v_new*$v_new);
    $rd_new = 1 / sqrt(1/($rd_new*$rd_new) + 1/$v);
    my $r_new = $team_r_1_g2 + $rd_new*$rd_new*$gv*($team_points_1 - $ev);
    print "testing #### : ".$rd_new*$rd_new*$gv*($team_points_1 - $ev)."\n" ;
    print "testing #### : $rd_new*$rd_new*$gv*($team_points_1 - $ev)\n" ;
    $r_new = int (173.7178*$r_new + 1500);
    $rd_new = $rd_new*173.7178;
    $matches++;

    print "$v_new $r_new $rd_new \n";
    $ratings{$team_name_1}{rating} = $r_new;
    $ratings{$team_name_1}{deviation} = $rd_new;
    $ratings{$team_name_1}{volatility} = $v_new;
    $ratings{$team_name_1}{matches}++;
    print "\n\n";
  }
  my $matches = 0;
  my $params_1 = {
    team_r_1 => exists $ratings{$team_name_1} ? $ratings{$team_name_1}{rating} : 1500,
    team_rd_1 => exists $ratings{$team_name_1} ? $ratings{$team_name_1}{deviation} : 350,
    team_v_1 => exists $ratings{$team_name_1} ? $ratings{$team_name_1}{volatility} : $volatility,
    team_r_2 => exists $ratings{$team_name_2} ? $ratings{$team_name_2}{rating} : 1500,
    team_rd_2 => exists $ratings{$team_name_2} ? $ratings{$team_name_2}{deviation} : 350,
    team_v_2 => exists $ratings{$team_name_2} ? $ratings{$team_name_2}{volatility} : $volatility,
    matches => exists $ratings{$team_name_2} ? $ratings{$team_name_2}{matches} : 0
  };
  my $params_2 = {
    team_r_2 => exists $ratings{$team_name_1} ? $ratings{$team_name_1}{rating} : 1500,
    team_rd_2 => exists $ratings{$team_name_1} ? $ratings{$team_name_1}{deviation} : 350,
    team_v_2 => exists $ratings{$team_name_1} ? $ratings{$team_name_1}{volatility} : $volatility,
    team_r_1 => exists $ratings{$team_name_2} ? $ratings{$team_name_2}{rating} : 1500,
    team_rd_1 => exists $ratings{$team_name_2} ? $ratings{$team_name_2}{deviation} : 350,
    team_v_1 => exists $ratings{$team_name_2} ? $ratings{$team_name_2}{volatility} : $volatility
  };
  calculate_rating($team_name_1, $team_score_1, $team_name_2, $team_score_2, $params_1);
  calculate_rating($team_name_2, $team_score_2, $team_name_1, $team_score_1, $params_2);
}

my $insert_ratings = $dbh->prepare("INSERT INTO ratings_overall VALUES (?,?,?,?,?)");
$dbh->begin_work();
$dbh->do('DELETE FROM ratings_overall');
for my $team (keys %ratings){
  $insert_ratings->execute($team, $ratings{$team}{rating}, $ratings{$team}{deviation}, $ratings{$team}{volatility}, $ratings{$team}{matches});
}
$dbh->commit();
