use warnings;
use strict;

use DBI;
use LWP::UserAgent;
use HTML::TreeBuilder;
use Data::Dumper;

my $dbh = DBI->connect("dbi:mysql:dbname=taterset_dota2;hostname=localhost;port=3306","taterset_james","4D}f;%x~^g1#",{ 
  RaiseError=>1, 
  PrintError=>0
  });
my $ua = LWP::UserAgent->new();
$ua->agent('Mozilla/5.0');


my $url = 'http://www.datdota.com/matches.php';
my $max_offset = 17000;
$dbh->begin_work();
eval {
  for (my $i = 0; $i <= $max_offset; $i += 500){
    my $response = $ua->get($url."?l0=$i");

    if ($response->is_success){
      my $tree = HTML::TreeBuilder->new;
      $tree->parse($response->decoded_content);
      $tree = $tree->elementify();
      my @matches = $tree->look_down( _tag=>'tbody')->look_down(_tag=>'tr');
      my $stmt = $dbh->prepare("INSERT INTO matches (match_id, team_name_1, team_score_1, team_name_2, team_score_2, date) VALUES (?,?,?,?,?,?)");
      foreach my $match (@matches){
        my ($matchid, $date, $tournament, $radiant, $dire, $winner, $round, $game, $time, $score) = $match->look_down(_tag=>'td');
        my @date_splits = split('/', $date->as_text);
        my $date_int = (2000 + $date_splits[2])*10000 + $date_splits[0]*100 + $date_splits[1];
        $stmt->execute(
            $matchid->as_text, 
            $radiant->as_text, 
            $winner->as_text eq 'RADIANT' ? 1 : 0,
            $dire->as_text,
            $winner->as_text eq 'DIRE' ? 1 : 0,
            $date_int
            );
        print "$date_int ".$radiant->as_text." ".$dire->as_text." ".$winner->as_text."\n";

      }

    } else {
      print $response->status_line."\n";
      print $response->decoded_content;
    }
  }
};
$dbh->commit();
