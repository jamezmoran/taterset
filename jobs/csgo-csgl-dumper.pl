use warnings;
use strict;

use DBI;
use LWP::UserAgent;
use HTML::TreeBuilder;
use Data::Dumper;
use Math::Trig;
use List::Util qw/min/;
use Config::Tiny;

sub g {
  my $o = shift;
  return 1 / sqrt(1 + 3*$o*$o/(pi*pi));
}
sub E {
  my ($u, $uj, $oj) = @_;
  return 1/(1 + exp(0 - g($oj)*($u-$uj)));
}

sub deviation_to_stars {
  my $deviation = shift;
  my $stars = 0;
  if ($deviation < 60 ){
    $stars++;
  }
  if ($deviation < 55){
    $stars++;
  }
  if ($deviation < 50){
    $stars++;
  }
  if ($deviation < 45){
    $stars++;
  }
  if ($deviation < 40){
    $stars++;
  }
  return $stars;
}

my $config = Config::Tiny->read('/home/taterset/dbconfig.ini');
my $dbh = DBI->connect('dbi:mysql:dbname='.$config->{config_csgo}->{dbname}.';hostname=localhost;port=3306',$config->{config_csgo}->{user},$config->{config_csgo}->{pass},{ RaiseError=>1, PrintError=>0});

my $ua = LWP::UserAgent->new();
$ua->agent('Mozilla/5.0');
my $url = 'http://csgolounge.com/';
my $response = $ua->get($url);

if ($response->is_success){
  $dbh->begin_work();
  $dbh->do("DELETE FROM upcoming_matches");
  $dbh->do("DELETE FROM live_matches");
  $dbh->do("DELETE FROM past_matches");
  my $insert_stmt_upcoming = $dbh->prepare("INSERT INTO upcoming_matches (team_1, team_2, best_of, time, our_odds, csgl_odds, csgl_link, hltv_link,stars) VALUES (?,?,?,?,?,?,?,?,?)");
  my $insert_stmt_live = $dbh->prepare("INSERT INTO live_matches (team_1, team_2, best_of, time, our_odds, csgl_odds, csgl_link, hltv_link,stars) VALUES (?,?,?,?,?,?,?,?,?)");
  my $insert_stmt_past = $dbh->prepare("INSERT INTO past_matches (winner, team_1, team_2, best_of, time, our_odds, csgl_odds, csgl_link, hltv_link,stars) VALUES (?,?,?,?,?,?,?,?,?,?)");
  my $select_stmt = $dbh->prepare(
    "SELECT team.team_name, rating, rating_deviation, matches 
    FROM ratings_overall
    INNER JOIN team ON team.id=ratings_overall.team_id
    INNER JOIN team_names ON team_names.team_id=ratings_overall.team_id 
    WHERE team_names.team_name LIKE ?");

  my $tree = HTML::TreeBuilder->new;
  $tree->parse($response->decoded_content);
  my @matches = $tree->look_down(_tag=>'div', class=>'matchmain');
  foreach my $match (@matches){
    my $match_header = $match->look_down(_tag=>'div', class=>'matchheader');
    my $live = 0;
    my $upcoming = 0;
    my $past = 0;
    my $winner = 0;
    my $match_header_text = $match_header->look_down('class'=>'whenm')->as_text;
    if ($match_header_text =~ /from now/){
      $upcoming = 1;
    } elsif ($match_header_text =~ / LIVE/){
      $live = 1;
    } else {
      $past = 1;
    }
    my $match_href = $match->look_down(_tag=>'a', 'href'=>qr/match/);
    if (not defined $match_href){
      next;
    }
    my $csgl_link = $url.($match_href->attr('href'));
    my @teamtexts = $match->look_down(class=>'teamtext');
    my @teams = $match->look_down(class=>'team');
    if ($teams[0]->look_down(_tag=>'img', src=>qr/\/won\.png/)){
      $winner = 1;
    } else {
      $winner = 2;
    }
    my $team_1_name = $teamtexts[0]->look_down(_tag=>'b')->as_text;
    my $team_1_odds = $teamtexts[0]->look_down(_tag=>'i')->as_text;
    $team_1_odds =~ s/%//;
    my $team_2_name = $teamtexts[1]->look_down(_tag=>'b')->as_text;
    my $best_of = $match->look_down(_tag=>'span', class=>'format');
    if (defined $best_of){
      $best_of = $best_of->as_text;
    } else {
      next;
    }
    $best_of =~ s/BO//;

    $team_1_name =~ s/-/-/g;
    $team_2_name =~ s/-/-/g;

    $select_stmt->execute($team_1_name);
    if ($select_stmt->rows() == 0){
      #print STDERR "Cannot find team $team_1_name in database.\n";
      next;
    }
    my ($team_1_realname, $rating_1, $deviation_1, $matches_1) = $select_stmt->fetchrow_array;

    $select_stmt->execute($team_2_name);
    if ($select_stmt->rows() == 0){
      #print STDERR "Cannot find team $team_2_name in database.\n";
      next;
    }
    my ($team_2_realname, $rating_2, $deviation_2, $matches_2) = $select_stmt->fetchrow_array;

    $rating_1 = ($rating_1 - 1500) / 173.7178;
    $rating_2 = ($rating_2 - 1500) / 173.7178;
    $deviation_2 = $deviation_2 / 173.7178;
    my $our_odds = -1;
    my $bo1_win = E($rating_1, $rating_2, $deviation_2);
    if ($best_of == 1){
      $our_odds = $bo1_win*100;
    } 
    elsif ($best_of == 2){
      my $t1_win = $bo1_win**2;
      my $t2_win = (1-$bo1_win)**2;
      $our_odds = 100 * $t1_win / ($t1_win + $t2_win);
    }
    elsif ($best_of == 3){
      $our_odds = (($bo1_win**3)
        + 3*($bo1_win**2 * (1-$bo1_win))) * 100;
    }
    elsif ($best_of == 5){
      $our_odds = (10 * ($bo1_win**3 * (1-$bo1_win)**2)
        + 5 * ($bo1_win**4 * (1-$bo1_win))
        + $bo1_win**5)*100;
    } else {
      next;
    }

    my $stars_1 = deviation_to_stars($deviation_1);
    my $stars_2 = deviation_to_stars($deviation_2*173.7178);
    if ($upcoming){
      $insert_stmt_upcoming->execute($team_1_realname, $team_2_realname, $best_of, '2015-01-01', $our_odds, $team_1_odds, $csgl_link, 'dummy_url', min($stars_1,$stars_2));
    } elsif ($live){
      $insert_stmt_live->execute($team_1_realname, $team_2_realname, $best_of, '2015-01-01', $our_odds, $team_1_odds, $csgl_link, 'dummy_url', min($stars_1,$stars_2));
    } elsif ($past){
      $insert_stmt_past->execute($winner, $team_1_realname, $team_2_realname, $best_of, '2015-01-01', $our_odds, $team_1_odds, $csgl_link, 'dummy_url', min($stars_1,$stars_2));
    }
  }
  $dbh->commit();
} else {
  print STDERR $response->status_line if (not $response->is_server_error());
}
