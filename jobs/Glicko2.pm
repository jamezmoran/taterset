use warnings;
use strict;
package Glicko2;

use Storable qw/dclone/;
use DBI;
use POSIX;
use Math::Trig;
use List::Util qw/min max/;

sub new {
  my $class = shift;
  my $database = 'taterset_csgo';
  my $hostname = 'localhost';
  my $dsn = "dbi:mysql:database=$database;host=$hostname;port=3306";
  my $attr = { 
    _dbh => DBI->connect($dsn,"taterset_james","4D}f;%x~^g1#")
  };
  return bless $attr, $class;
}

my $c = 0.5;
my $default_rating = 1500;
my $default_deviation = 350;
my $default_volatility = 0.05;

sub _g {
  my ($rd) = @_;
  return 1/sqrt(1 + 3*($rd**2)/(pi**2));
}

sub _E {
  my ($r, $rj, $rdj) = @_;
  return 1/(1+exp(0 - _g($rdj)*($r-$rj)));
}

sub _f {
  my ($x, $delta, $rd, $v, $a) = @_;
  return exp($x)*($delta**2 - $rd**2 - $v - exp($x))/(2*(($rd**2 + $v + exp($x))**2)) - ($x - $a)/($c**2);
}

sub calculate {
  my ($self) = @_;
  my $dbh = $self->{_dbh};
  my $now = time;
  my $six_months_ago_unix = $now - 2*7.88923e6;
  my $day_secs = 86400;
  my $six_months_ago_str = strftime("%Y-%m-%d", localtime($six_months_ago_unix));
  my %teams;
  my $teams_stmt = $dbh->prepare(
    "SELECT DISTINCT * 
    FROM (
      SELECT team_1 as team FROM `matches` WHERE date > '$six_months_ago_str'
      UNION 
      SELECT team_2 FROM `matches` WHERE date > '$six_months_ago_str'
    ) as T"
  );
  $teams_stmt->execute();
  while (my ($team) = $teams_stmt->fetchrow_array){
    $teams{$team}{rating} = $default_rating;
    $teams{$team}{deviation} = $default_deviation;
    $teams{$team}{volatility} = $default_volatility;
    $teams{$team}{matches} = 0;
  }
  for (my $from_date = $six_months_ago_unix; $from_date < $now; $from_date += $day_secs*5){
    my %curr_teams = %{dclone(\%teams)};
    my $next_date = $from_date + $day_secs*5;
    my $from_date_str = strftime("%Y-%m-%d", localtime($from_date));
    my $next_date_str = strftime("%Y-%m-%d", localtime($next_date));
    print "##################### $from_date_str to $next_date_str #######################\n";
    my $matches_stmt = $dbh->prepare(
      "SELECT *
      FROM matches
      WHERE 
        date > '$from_date_str' 
        AND date <= '$next_date_str'
        AND (team_1 = ? OR team_2 = ?)
      ORDER BY date ASC"
    );

    foreach my $team (keys %curr_teams){
      $curr_teams{$team}{rating} = ($curr_teams{$team}{rating}-1500)/173.7178;
      $curr_teams{$team}{deviation} = $curr_teams{$team}{deviation}/173.7178;
    }

    foreach my $team (keys %curr_teams){
      my $own_rating = $curr_teams{$team}{rating};
      my $own_rd = $curr_teams{$team}{deviation};
      my $own_vol = $curr_teams{$team}{volatility};
      $matches_stmt->execute($team,$team);

      my $v_sum = 0;
      my $delta_sum = 0;
      while (my ($id, $map, $team_1, $team_score_1, $team_2, $team_score_2, $hltv_link) = $matches_stmt->fetchrow_array){
        my ($own_game_score, $enemy_game_score, $enemy_id);
        if ($team_1 == $team){
          $own_game_score = $team_score_1;
          $enemy_game_score = $team_score_2;
          $enemy_id = $team_2;
        } else {
          $own_game_score = $team_score_2;
          $enemy_game_score = $team_score_1;
          $enemy_id = $team_1;
        }
        my $own_score = $own_game_score > $enemy_game_score ? 1 : 0;
        if ($own_score == 1 ){
          my $score_difference = $own_game_score - $enemy_game_score;
          if ($score_difference < 3
            || ($own_game_score >= 15 && $enemy_game_score >= 15)){
            $own_score = 0.70;
          } elsif ($score_difference <= 10){
            $own_score = 0.85;
          } else {
            $own_score = 1;
          }
        } else {
          my $score_difference = $enemy_game_score - $own_game_score;
          if ($score_difference < 3
            || ($own_game_score >= 15 && $enemy_game_score >= 15)){
            $own_score = 0.30;
          } elsif ($score_difference <= 10){
            $own_score = 0.15;
          } else {
            $own_score = 0;
          }
        }
        my $enemy_score = 1 - $own_score;
        my $enemy_rating = $curr_teams{$enemy_id}{rating};
        my $enemy_rd = $curr_teams{$enemy_id}{deviation};

        $v_sum += (_g($enemy_rd)**2)
          * _E($own_rating,$enemy_rating,$enemy_rd)
          * (1 - _E($own_rating,$enemy_rating,$enemy_rd));
        $delta_sum += _g($enemy_rd)*($own_score - _E($own_rating, $enemy_rating, $enemy_rd));

        $teams{$team}{matches}++;
      }
      if ($v_sum != 0 && $delta_sum !=0){
        my $v = 1/$v_sum;
        my $delta = $v*$delta_sum;

        my $A = log($own_vol**2);
        my $a = $A;
        my $B;
        if ($delta**2 > $own_rd**2 + $v){
          $B = log($delta**2 - $own_rd**2 - $v);
        } else {
          my $k = 1;
          while(_f($a - $k*$c, $delta, $own_rd, $v, $a) < 0){
            ++$k;
          }
          $B = $a - $k*$c;
        }

        my $fa = _f($A, $delta, $own_rd, $v, $a);
        my $fb = _f($B, $delta, $own_rd, $v, $a);

        while (abs($B - $A) > 0.000001){
          my $C = $A + ($A-$B)*$fa/($fb-$fa);
          my $fc = _f($C, $delta, $own_rd, $v, $a);
          if ($fc < 0){
            $A = $B;
            $fa = $fb
          } else {
            $fa = $fa/2;
          }
          $B = $C;
          $fb = $fc;
        }
        my $vol_prime = exp($A/2);
        my $rd_old = sqrt($own_rd**2 + $vol_prime**2);

        my $rd_prime = 1/sqrt(1/($rd_old**2) + 1/$v);
        my $r_prime = $own_rating + $rd_prime**2 * $delta_sum;

        $teams{$team}{rating} = $r_prime*173.7178 + 1500;
        $teams{$team}{deviation} = $rd_prime*173.7178;
        $teams{$team}{volatility} = $vol_prime;
        $teams{$team}{time_since_last_match} = 1;
      } else {
        my $rd_prime = sqrt($own_rd**2 + $own_vol**2);
        $teams{$team}{deviation} = $rd_prime*173.7178;
      }
    }
  }

  my $insert_ratings = $dbh->prepare("INSERT INTO ratings_overall VALUES (?,?,?,?,?)");
  $dbh->begin_work();
  $dbh->do('DELETE FROM ratings_overall');
  for my $team (keys %teams){
    $insert_ratings->execute($team, $teams{$team}{rating}, $teams{$team}{deviation}, $teams{$team}{volatility}, $teams{$team}{matches});
  }
  $dbh->commit();
}
