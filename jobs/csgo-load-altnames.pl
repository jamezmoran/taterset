use warnings;
use strict;

use DBI;
use Data::Dumper;
use Config::Tiny;

my $config = Config::Tiny->read('/home/taterset/jobs/taterset/alt_team_names.ini');
print Dumper($config);
my %altnames;
foreach my $team (keys %$config){
  my @teams = split(',',$config->{$team}->{altnames});
  foreach my $team_split (@teams){
    $altnames{$team_split} = $team;
  }
}
print Dumper(\%altnames);

my $dbh = DBI->connect("dbi:mysql:dbname=taterset_csgo;hostname=localhost;port=3306","taterset_james","4D}f;%x~^g1#",{ RaiseError=>1, PrintError=>0});
$dbh->begin_work;
my $insert_name = $dbh->prepare("INSERT IGNORE INTO team_names VALUES (?,?)");
my $select_team = $dbh->prepare("SELECT id FROM team WHERE team_name=?");
foreach my $altname (keys %altnames){
  eval {
    $select_team->execute($altnames{$altname});
    my ($team_id) = $select_team->fetchrow_array;
    $insert_name->execute($altname, $team_id);
  };
  if ($@){
    print STDERR $@;
  }
}
$dbh->commit();
