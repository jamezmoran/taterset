use warnings;
use strict;
package Glicko;

use DBI;
use POSIX;
use Math::Trig;
use List::Util qw/min max/;

sub new {
  my $class = shift;
  my $database = 'taterset_csgo';
  my $hostname = 'localhost';
  my $dsn = "dbi:mysql:database=$database;host=$hostname;port=3306";
  my $attr = { 
    _dbh => DBI->connect($dsn,"taterset_james","4D}f;%x~^g1#")
  };
  return bless $attr, $class;
}

my $c = 15;

sub _g {
  my ($rd) = @_;
  return 1/sqrt(1 + 3*(0.0057565**2)*($rd**2)/(pi**2));
}

sub _E {
  my ($r, $rj, $rdj) = @_;
  return 1/(1+10**(0 - _g($rdj)*($r-$rj)/400));
}

sub calculate {
  my ($self) = @_;
  my $dbh = $self->{_dbh};
  my $now = time;
  my $six_months_ago_unix = $now - 2*7.88923e6;
  my $week_secs = 604800;
  my $six_months_ago_str = strftime("%Y-%m-%d", localtime($six_months_ago_unix));
  my %teams;
  my $teams_stmt = $dbh->prepare(
    "SELECT DISTINCT * 
    FROM (
      SELECT team_1 as team FROM `matches` WHERE date > '$six_months_ago_str'
      UNION 
      SELECT team_2 FROM `matches` WHERE date > '$six_months_ago_str'
    ) as T"
  );
  $teams_stmt->execute();
  while (my ($team) = $teams_stmt->fetchrow_array){
    $teams{$team}{rating} = 1500;
    $teams{$team}{rating_deviation} = 350;
    $teams{$team}{matches} = 0;
    $teams{$team}{time_since_last_match} = 1;
  }
  for (my $from_date = $six_months_ago_unix; $from_date < $now; $from_date += $week_secs){
    my %curr_teams = %teams;
    my $next_date = $from_date + $week_secs;
    my $from_date_str = strftime("%Y-%m-%d", localtime($from_date));
    my $next_date_str = strftime("%Y-%m-%d", localtime($next_date));
    my $matches_stmt = $dbh->prepare(
      "SELECT *
      FROM matches
      WHERE 
        date > '$from_date_str' 
        AND date <= '$next_date_str'
        AND (team_1 = ? OR team_2 = ?)
      ORDER BY date ASC"
    );

    foreach my $team (keys %curr_teams){
      my $rd_old = $curr_teams{$team}{rating_deviation};
      my $time_since = $curr_teams{$team}{time_since_last_match};
      $curr_teams{$team}{rating_deviation} = min(sqrt($rd_old**2 + ($c**2)*$time_since),350);
    }

    foreach my $team (keys %curr_teams){
      my $own_rating = $curr_teams{$team}{rating};
      my $own_rd = $curr_teams{$team}{rating_deviation};
      $matches_stmt->execute($team,$team);
      if ($matches_stmt->rows() == 0){
        $teams{$team}{time_since_last_match}++;
        next;
      }

      my $r_sum = 0;
      my $d2_sum = 0;
      while (my ($id, $map, $team_1, $team_score_1, $team_2, $team_score_2, $hltv_link) = $matches_stmt->fetchrow_array){
        my ($own_game_score, $enemy_game_score, $enemy_id);
        if ($team_1 == $team){
          $own_game_score = $team_score_1;
          $enemy_game_score = $team_score_2;
          $enemy_id = $team_2;
        } else {
          $own_game_score = $team_score_2;
          $enemy_game_score = $team_score_1;
          $enemy_id = $team_1;
        }
        my $own_score = $own_game_score > $enemy_game_score ? 1 : 0;
        my $enemy_score = $own_game_score < $enemy_game_score ? 1 : 0;
        my $enemy_rating = $curr_teams{$enemy_id}{rating};
        my $enemy_rd = $curr_teams{$enemy_id}{rating_deviation};

        $r_sum += _g($enemy_rd)*($own_score - _E($own_rating, $enemy_rating, $enemy_rd));
        $d2_sum += (_g($enemy_rd)**2)
          * _E($own_rating,$enemy_rating,$enemy_rd)
          * (1 - _E($own_rating,$enemy_rating,$enemy_rd));

        $teams{$team}{matches}++;
      }
      my $q = 0.0057565;
      my $d2 = 1/(($q**2)*$d2_sum);
      my $r_prime = $own_rating + $q/(1/($own_rd**2) + 1/$d2)*$r_sum;
      my $rd_prime = sqrt(1/(1/($own_rd**2) + 1/$d2));
      $teams{$team}{rating} = $r_prime;
      $teams{$team}{rating_deviation} = $rd_prime;
      $teams{$team}{time_since_last_match} = 1;
    }
  }
  my $insert_ratings = $dbh->prepare("INSERT INTO ratings_overall VALUES (?,?,?,?,?)");
  $dbh->begin_work();
  $dbh->do('DELETE FROM ratings_overall');
  for my $team (keys %teams){
    $insert_ratings->execute($team, $teams{$team}{rating}, $teams{$team}{rating_deviation}, 0, $teams{$team}{matches});
  }
  $dbh->commit();
}
